package com.salsify.lineserver.linereader;

import java.io.File;
import java.util.Map;

public class ProcessmentResult {
	public Map<Integer, File> archive;
	public int totalLines;
	
	public ProcessmentResult(Map<Integer, File> archive, int totalLines) {
		this.archive = archive;
		this.totalLines = totalLines;
	}

}
