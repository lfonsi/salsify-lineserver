package com.salsify.lineserver.linereader;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

public class LineReader {

	private Map<Integer, File> archive;
	private int linesPerFile;
	private int totalNrLines;

	@Inject
	public LineReader(Map<Integer, File> archive, @Named("linesPerFile") int linesPerFile,
			@Named("totalNrLines") int totalNrLines) throws IOException {
		this.archive = archive;
		this.linesPerFile = linesPerFile;
		this.totalNrLines = totalNrLines;
	}

	/**
	 * Gets the line that is on line <strong>lineNumber</strong> on the original
	 * file
	 * 
	 * @param lineNumber the number of the line to return
	 * @return the contents of line <strong>lineNumber</strong>
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	public String getLine(int lineNumber) throws IllegalArgumentException, IOException {
		if (lineNumber <= 0 || lineNumber > totalNrLines) {
			throw new IllegalArgumentException("Invalid line number provided");
		}
		File file = getFile(lineNumber);

		// on the partial file, the line to retrieve is given by the calculatedLineNr
		// variable
		int calculatedLineNr = calculateLineNumber(lineNumber);

		// skips the first calculatedLineNr - 1 lines, and returns the specified line
		return Files.lines(file.toPath(), Charset.forName("ASCII")).skip(calculatedLineNr - 1).findFirst().get();
	}

	/**
	 * Gets the file that contains <strong>lineNumber</strong> <br/>
	 * <br/>
	 * 
	 * <strong>Example:</strong> <br/>
	 * <br/>
	 * 
	 * for <strong>lineNumber</strong>=49 and <strong>linesPerFile</strong>=20 the
	 * formula <strong>lineNumber / linesPerFile + (lineNumber % linesPerFile == 0 ?
	 * 0 : 1)</strong> is applied: <br/>
	 * <br/>
	 * 
	 * 49 / 20 + (49 % 20 == 0? 0 : 1) = 2 + 1 = 3 <br/>
	 * <br/>
	 * 
	 * This means that the file where line number 49 is, corresponds to file stored
	 * in the archive with key=3
	 * 
	 * @param lineNumber the requested line number
	 * @return the file reference that contains the requested line number
	 */
	private File getFile(int lineNumber) {
		int key = lineNumber / linesPerFile + (lineNumber % linesPerFile == 0 ? 0 : 1);

		return archive.get(key);
	}

	/**
	 * Based on the requested line number returns the effective line number to
	 * retrieve on the partial file using the formula <br>
	 * <br>
	 * lineNumber - ((lineNumber / linesPerFile) - (lineNumber % linesPerFile == 0 ?
	 * 1 : 0)) * linesPerFile <br/>
	 * <br/>
	 * 
	 * In a file with 500 lines, where each partial file has 100 files (total of 5
	 * partial files), <strong>lineNumber=401</strong> is on file 5 and corresponds
	 * to line 1 of that file: <br/>
	 * <br/>
	 * 
	 * 401 - ((401/100) - (401 % 100 == 0 ? 1 : 0)) * 100 = 401 - (4 - 0) * 100 =
	 * 401 - 400 = 1
	 * 
	 * @param lineNumber the requested line number
	 * @return line number on the partial file that corresponds to the requested
	 *         line number
	 */
	private int calculateLineNumber(int lineNumber) {
		return lineNumber - ((lineNumber / linesPerFile) - (lineNumber % linesPerFile == 0 ? 1 : 0)) * linesPerFile;
	}
}
