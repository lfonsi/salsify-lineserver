package com.salsify.lineserver.linereader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

public class FileProcessor {

	/**
	 * Preprocesses the file given as the program argument into smaller files so
	 * that the processing is faster
	 * 
	 * @param fileToProcess
	 * @param maxFileLines  the max number of lines each smaller file can have
	 * @return map with reference to all the created files
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public static ProcessmentResult preProcessFile(File fileToProcess, int maxFileLines) throws IOException {

		Preconditions.checkArgument(fileToProcess.isFile(), "invalid file path provided %s",
				fileToProcess.getAbsolutePath());

		// holder of the common name part for all generated files during the
		// preprocessing phase
		String partialFileName = fileToProcess.getParent() + File.separator + fileToProcess.getName() + "_";

		// keeps a reference of the current line on a file
		int currentLine = 0;

		// keeps a reference to the file index (used as key for the
		// 'archive' map)
		int currentFileIndex = 1;

		// stores the total number of lines on the original file
		int totalLines = 0;

		// keeps references for the smaller files
		Map<Integer, File> archive = Maps.newConcurrentMap();

		InputStreamReader is = new InputStreamReader(new FileInputStream(fileToProcess));
		BufferedWriter out;

		try (BufferedReader in = new BufferedReader(is)) {
			String line;

			// creates reference for the new file
			File newFile = new File(partialFileName + currentFileIndex);

			System.out.println("Started processing file " + newFile.getName());

			out = getNewFileWriter(newFile);

			/**
			 * while the original file has lines to read, each line will be written to the
			 * current file -> newFile variable
			 * 
			 * the currentLine variable is incremented every time a line is written, and
			 * will be used to decide when a new file needs to be created
			 * 
			 * totalLines variable stores the total lines of the original file and will be
			 * incremented every time a new line is read. This saves processing time, as
			 * otherwise another iteration over all the lines would need to be made just to
			 * retrieve the number of lines
			 */
			while ((line = in.readLine()) != null) {
				writeLine(line, out);
				currentLine++;
				totalLines++;

				// if max number of lines per file is reached, store reference for
				// the file in 'archive' map and create a new file
				if (currentLine == maxFileLines) {
					out.close();
					currentLine = 0;
					archive.put(currentFileIndex++, newFile);

					// checks if a new file needs to be created
					if ((line = in.readLine()) != null) {

						System.out.println("Ended processing file " + newFile.getName());

						// creates reference for a new file
						newFile = new File(partialFileName + currentFileIndex);

						System.out.println("Started processing file " + newFile.getName());

						out = getNewFileWriter(newFile);

						// writes the new line into the new file
						writeLine(line, out);
						currentLine++;
						totalLines++;
					}
				}
			}

			out.close();

			// adds the last generated file to the archive
			archive.put(currentFileIndex, newFile);
		}

		return new ProcessmentResult(archive, totalLines);
	}

	/**
	 * Gets a new {@link java.io#BufferedWriter} for a given file
	 * 
	 * @param file the file for which to get a FileWriter
	 * @return
	 * @throws IOException
	 */
	private static BufferedWriter getNewFileWriter(File file) throws IOException {
		return new BufferedWriter(new FileWriter(file));
	}

	/**
	 * Writes a new line
	 * 
	 * @param line   the line to write
	 * @param writer the writer used to make the write
	 * @throws IOException
	 */
	private static void writeLine(String line, BufferedWriter writer) throws IOException {
		writer.write(line);
		writer.newLine();
	}
}
