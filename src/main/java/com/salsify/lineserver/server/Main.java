package com.salsify.lineserver.server;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Map;

import javax.servlet.DispatcherType;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.glassfish.jersey.servlet.ServletContainer;

import com.google.inject.Guice;
import com.google.inject.servlet.GuiceFilter;
import com.salsify.lineserver.linereader.FileProcessor;
import com.salsify.lineserver.linereader.ProcessmentResult;
import com.salsify.lineserver.server.servlet.SalsifyServletModule;

public class Main {
	private static int MAX_LINES_PER_FILE = 10000;

	public static void main(String[] args) throws IOException {

		String fileToProcess = args[0];
		int linesPerFile;

		try {
			linesPerFile = args.length > 1 ? Integer.parseInt(args[1]) : MAX_LINES_PER_FILE;
		} catch (NumberFormatException e) {
			// in case the second program argument is not a number, use a default value
			linesPerFile = MAX_LINES_PER_FILE;
		}

		System.out.println("Starting file preprocessing...");
		ProcessmentResult processResult = FileProcessor.preProcessFile(new File(fileToProcess), linesPerFile);
		System.out.println("File preprocessing terminated");
		
		System.out.println("Starting server...");

		// GUICE, JETTY and JERSEY configuration
		// based on
		// http://zetcode.com/articles/jerseyembeddedjetty/
		// http://blog.timmattison.com/archives/2014/09/02/full-example-code-showing-how-to-use-guice-and-jetty/
		Guice.createInjector(new SalsifyServletModule(processResult.archive, linesPerFile, processResult.totalLines));

		Server server = new Server(8888);

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
		context.setContextPath("/");
		context.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
		server.setHandler(context);

		context.addServlet(ServletContainer.class, "/*");

		try {
			server.start();
			server.join();
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			server.destroy();
		}
	}

}
