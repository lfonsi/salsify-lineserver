package com.salsify.lineserver.server.servlet;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.google.inject.servlet.ServletModule;
import com.salsify.lineserver.linereader.LineReader;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class SalsifyServletModule extends ServletModule {

	private Map<Integer, File> archive;
	private int linesPerFile;
	private int totalNrLines;

	public SalsifyServletModule(Map<Integer, File> archive, int linesPerFile, int totalNrLines) {
		super();
		this.archive = archive;
		this.linesPerFile = linesPerFile;
		this.totalNrLines = totalNrLines;
	}

	@Override
	protected void configureServlets() {
		ResourceConfig rc = new PackagesResourceConfig("com.salsify.lineserver.rest");
		for (Class<?> resource : rc.getClasses()) {
			bind(resource);
		}

		bind(LineReader.class).in(Singleton.class);
		bind(Integer.class).annotatedWith(Names.named("linesPerFile")).toInstance(this.linesPerFile);
		bind(Integer.class).annotatedWith(Names.named("totalNrLines")).toInstance(this.totalNrLines);

		serve("/*").with(GuiceContainer.class);
	}

	@Provides
	public Map<Integer, File> getArchive() throws IOException {
		return archive;
	}
}
