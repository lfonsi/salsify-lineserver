package com.salsify.lineserver.rest;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.salsify.lineserver.linereader.LineReader;

@Path("/")
public class Salsify {

	private final LineReader reader;

	@Inject
	public Salsify(LineReader reader) {
		this.reader = reader;
	}

	@GET
	@Path("/lines/{lineNr}")
	public Response getLine(@PathParam(value = "lineNr") int lineNr) throws IOException {
		try {

			String message = reader.getLine(lineNr);

			return Response.status(200).entity(message).build();
		} catch (IllegalArgumentException e) {
			return Response.status(413).build();
		}
	}

}
