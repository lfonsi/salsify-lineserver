package com.salsify.lineserver;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.servlet.GuiceFilter;
import com.salsify.lineserver.linereader.FileProcessor;
import com.salsify.lineserver.linereader.ProcessmentResult;
import com.salsify.lineserver.server.servlet.SalsifyServletModule;

public class SalsifyServerTest {

	private static Server server;
	private static URI serverUri;
	private static int PORT = 8889;

	@BeforeClass
	public static void initServer() throws Exception {
		String path = SalsifyServerTest.class.getClassLoader().getResource("data/500000_lines_file.txt").getFile();
		server = new Server(PORT);
		int linesPerFile = 10000;
		ProcessmentResult processmentResult = FileProcessor.preProcessFile(new File(path), linesPerFile);

		Guice.createInjector(new SalsifyServletModule(processmentResult.archive, linesPerFile, processmentResult.totalLines));

		ServletHolder defaultServ = new ServletHolder("default", DefaultServlet.class);
		defaultServ.setInitParameter("resourceBase", System.getProperty("user.dir"));
		defaultServ.setInitParameter("dirAllowed", "true");

		ServletContextHandler context = new ServletContextHandler();
		context.setContextPath("/");
		context.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));

		server.setHandler(context);

		context.addServlet(defaultServ, "/");

		server.start();

		serverUri = new URI(String.format("http://localhost:%d", PORT));
	}

	@Test()
	public void testValidRequest() throws MalformedURLException, IOException {
		HttpURLConnection connection = (HttpURLConnection) serverUri.resolve("/lines/5").toURL().openConnection();
		connection.connect();

		assertEquals(HttpStatus.OK_200, connection.getResponseCode());
	}

	@Test()
	public void testInvalidRequest() throws MalformedURLException, IOException {
		HttpURLConnection connection = (HttpURLConnection) serverUri.resolve("/lines/500001").toURL().openConnection();
		connection.connect();

		assertEquals(HttpStatus.PAYLOAD_TOO_LARGE_413, connection.getResponseCode());
	}

}
