package com.salsify.lineserver;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.salsify.lineserver.linereader.FileProcessor;
import com.salsify.lineserver.linereader.LineReader;
import com.salsify.lineserver.linereader.ProcessmentResult;

public class LineReaderTest {

	private File lines500000;
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	private LineReader reader;

	@Before
	public void before() throws Exception {
		int linesPerFile = 10000;
		this.lines500000 = new File(
				this.getClass().getClassLoader().getResource("data/500000_lines_file.txt").getFile());
		ProcessmentResult processmentResult = FileProcessor.preProcessFile(this.lines500000, 10000);
		reader = new LineReader(processmentResult.archive, linesPerFile, processmentResult.totalLines);
	}

	@Test()
	public void testNegativeLineNumber() throws IOException {
		thrown.expect(IllegalArgumentException.class);

		reader.getLine(-1);
	}

	@Test()
	public void testOutOfBoundsLineNumber() throws IOException {
		thrown.expect(IllegalArgumentException.class);

		reader.getLine(5000001);
	}

	@Test()
	public void testExistingLine() throws IOException {

		String line = reader.getLine(423);
		assertEquals("Line 423", line);
	}

	@Test()
	public void testBoundaryLine() throws IOException {
		// line 10000 is on partial file number 1 and line 10001 is on partial file
		// number 2
		assertEquals("Line 10000", reader.getLine(10000));
		assertEquals("Line 10001", reader.getLine(10001));
	}

}
