# README #

### 1) How does salsify-line server work? ###

To startup the system, run ./build.sh to build, followed by ./run.sh to start it.
eg:
> ./build.sh

> ./run.sh path_to_file number_of_lines

To startup the system, when running the run.sh script two arguments are provided:

- *path_to_file*: file path to the file being processed;
- *number_of_lines* (_optional, default=10000_): max number of lines for each file that will be created during the processment;

On start, the file is processed and split into several smaller files. This will help reduce the invoke time when finding a line.
The split process is explained in the comments on FileProcessor.class
The FileReader.class is responsible for finding which smaller file contains the requested line as well as the line to return on that smaller file.

### 2) How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file? ###

It is not expected for any file size that the system stops performing well.
Before starting the server, the most time consuming task is the file preprocessment.
Afterwards, all the operations are quite fast (map lookup and arithmetic calculations). The more time consuming task when retrieving a line is iterating through all
the lines on a smaller file, which takes O(n).

### 3) How will your system perform with 100 users? 10000 users? 1000000 users? ###

As the number of users increases, so does the response time. Please check 7).

### 4) What documentation, websites, papers, etc did you consult in doing this assignment? ###

For implementing Jersey in Jetty 
- <http://zetcode.com/articles/jerseyembeddedjetty/>

For using Guice with Jetty 
- <http://blog.timmattison.com/archives/2014/09/02/full-example-code-showing-how-to-use-guice-and-jetty/>

For finding the fastest way to read lines in Java:
- <http://nadeausoftware.com/articles/2008/02/java_tip_how_read_files_quickly>
- <https://www.baeldung.com/java-read-lines-large-file>

- StackOverflow :)

### 5) What third-party libraries or other tools does the system use? How did you choose each library or framework you used? ###
- Guice - for dependency injection
- Jersey - to implement REST
- Jetty - for the embedded server

I chose these libraries due to my previous experience using them and because I thought they were adequate for the given task.

### 6)How long did you spend on this exercise? ###

Around 8 hours doing research (including stuff that I later concluded wasn't useful for the application) and around 6/7 hours coding, testing/debugging, refactoring.

### 7) If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item? ###

In order:

- scalability: as the number of requests made increases, the system performance degrades. I would say scalability is the most important item to improve.
- lookup performance: I would implement a cache that would store a specified number of lines. Each request would look up the cache for the requested line.
If the line is in the cache, the response is immediate. Otherwise, it would process the request as the current solution, adding the line to the cache at the end.
- more test cases
- when no max number of lines per file is given as argument, instead of using the default value, I would try to dinamically compute a reasonable number based on the total number of lines.

### 8) If you were to critique your code, what would you have to say about it? ###
I tried for my code to be clean and easy to read, although there are parts that I can't simplify such as the file process code.
I could have made more tests, mainly on the server part.


